// const { direction } = require("html2canvas/dist/types/css/property-descriptors/direction");

const $ = data => document.querySelector(data);
const $$ = data => document.querySelectorAll(data);

//----------------------------------------------------------
// DRAG AND DROP
//----------------------------------------------------------
// FIRST TRY
/* const builder = $('#builder');
const result = $('#divForDropItems');

builder.addEventListener('dragstart', e => {
  e.dataTransfer.setData("text", e.target.id);
})
result.addEventListener('dragstart', e => {
  e.dataTransfer.setData("text", e.target.id);
})

builder.addEventListener('dragover', e => {
  e.preventDefault();
})
result.addEventListener('dragover', e => {
  e.preventDefault();
})

builder.addEventListener('drop', e => {
  e.preventDefault();
  const data = e.dataTransfer.getData("text");
  e.target.appendChild(document.getElementById(data));
})
result.addEventListener('drop', e => {
  e.preventDefault();
  const data = e.dataTransfer.getData("text");
  e.target.appendChild(document.getElementById(data));
}) */

const builder = $('#builder');
const result = $('#divForDropItems');
const images = $$('.clothesSelection img');
let currentDroppable = null;
let currentReset = null;
let drag = false;

const detectCollision = () => {
  console.clear();
  console.log("Collision!");
  document.onmousemove = function (e) {
    let element = document.getElementById(document.elementFromPoint(e.clientX, e.clientY).dataset.origin);
    let draggedImg = document.elementFromPoint(e.clientX, e.clientY);

    if (element != null && drag && currentReset) {
      draggedImg.style.position = "static";
      element.append(draggedImg);
      element = null;
      drag = false
    }
  }
}

images.forEach(image => {
  image.addEventListener('mousedown', e => {
    let shiftX = e.clientX - image.getBoundingClientRect().left;
    let shiftY = e.clientY - image.getBoundingClientRect().top;

    image.style.position = 'absolute';
    image.style.zIndex = 1000;
    document.body.append(image);

    moveAt(e.clientX, e.clientY);

    // moves the image at (clientX, clientY) coordinates
    // taking initial shifts into account
    function moveAt(posX, posY) {
      image.style.left = posX - shiftX + 'px';
      image.style.top = posY - shiftY + 'px';
    }

    function onMouseMove(event) {
      drag = true;
      moveAt(event.clientX, event.clientY);

      image.hidden = true;
      let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
      image.hidden = false;

      // mousemove events may trigger out of the window (when the ball is dragged off-screen)
      // if clientX/clientY are out of the window, then elementFromPoint returns null
      if (!elemBelow) return;

      // potential droppables are labeled with the class "droppable" (can be other logic)
      // the same gose for images that reset the drag
      let droppableBelow = elemBelow.closest('.droppable');
      let resetBelow = elemBelow.closest('.resetDrag');

      if (currentDroppable != droppableBelow || currentReset != resetBelow) {

        if (currentDroppable) {
          console.log("Bye");
          drag = false;
        }

        currentDroppable = droppableBelow;
        currentReset = resetBelow;

        if (currentDroppable || currentReset) {
          if (drag) {
            image.hidden = true;
            elemBelow.addEventListener('mouseenter', detectCollision());
            image.hidden = false;
            drag = false;
          }
        }
      }
    }

    // move the image on mousemove
    document.addEventListener('mousemove', onMouseMove);

    // drop the image, remove unneeded handlers
    image.onmouseup = function (e) {

      document.removeEventListener('mousemove', onMouseMove);
      image.onmouseup = null;

      let draggedImg = document.elementFromPoint(e.clientX, e.clientY);

      image.hidden = true;
      let elemBelow = document.elementFromPoint(e.clientX, e.clientY);
      image.hidden = false;

      switch (elemBelow.id) {
        case "bananaHead":
          if (draggedImg.dataset.origin == "clothesHead") {
            console.log("Head");
            draggedImg.style.position = "static";
            elemBelow.append(draggedImg);
          };
          break;

        case "bananaFace":
          if (draggedImg.dataset.origin == "clothesFace") {
            console.log("Face");
            draggedImg.style.position = "static";
            elemBelow.append(draggedImg);
          };
          break;

        case "bananaTorso":
          if (draggedImg.dataset.origin == "clothesTorso") {
            console.log("Torso");
            draggedImg.style.position = "static";
            elemBelow.append(draggedImg);
          };
          break;

        case "bananaFeet":
          if (draggedImg.dataset.origin == "clothesLowerBody") {
            console.log("Feet");
            draggedImg.style.position = "static";
            elemBelow.appendChild(draggedImg);
          };
          break;

        default:
          break;
      }

      drag = false;
    };

    image.ondragstart = function () {
      return false;
    };
  })
})
//----------------------------------------------------------

//----------------------------------------------------------
// CHANGE NAME OF BANANA
//----------------------------------------------------------
const bananaName = $('#bananaName');

$('.fa-pen-to-square').addEventListener('click', e => {
  changeBananaName(e);
})

const changeBananaName = e => {
  bananaName.innerHTML = `<label for="bananaNameInput">What's your bananas name?</label><br>
  <span>
    <input type="text" name="bananaNameInput" id="bananaNameInput">
    <i class="fa-solid fa-check-double" style="width:fit-content;"></i>
  </span>`;

  const nameChangeInput = $('#bananaNameInput');

  nameChangeInput.addEventListener('keypress', e => {
    if (e.key == "Enter") confirmBananaNameChange(e, nameChangeInput);
  })
  $('.fa-check-double').addEventListener('click', e => {
    confirmBananaNameChange(e, nameChangeInput);
  })
}

const confirmBananaNameChange = (e, nameChangeInput) => {
  if (nameChangeInput.value == "") {
    bananaName.innerHTML = `<span>Your Banana <i class="fa-regular fa-pen-to-square"></i></span>`;

    $('.fa-pen-to-square').addEventListener('click', e => {
      changeBananaName(e);
    })

  } else {
    bananaName.innerHTML = `<span>${nameChangeInput.value} <i class="fa-regular fa-pen-to-square"></i></span>`;

    $('.fa-pen-to-square').addEventListener('click', e => {
      changeBananaName(e);
    })
  }
}
//----------------------------------------------------------

//----------------------------------------------------------
// RANDOM BANANA PEEL SCATTER
//----------------------------------------------------------
const imageScatter = () => {
  const bananaImageUrl = "./media/icon/1920-banana-peel.png";
  const vpWidth = visualViewport.width;
  const vpHeight = visualViewport.height;
  const numOfImg = 10;

  for (let i = 0; i <= numOfImg; i++) {
    const img = document.createElement('img');
    img.src = bananaImageUrl;
    img.classList.add('resetDrag');

    img.setAttribute('style',
      `width: 100px;
      height: 100px;
      position: fixed;
      z-index: 100;
      top: ${Math.floor(Math.random() * vpHeight)}px;
      left: ${Math.floor(Math.random() * vpWidth)}px;`
    );

    img.addEventListener('mouseenter', detectCollision);

    $('body').appendChild(img);
  }
}
// imageScatter()
//----------------------------------------------------------

//----------------------------------------------------------
// FILL IMAGES IN BUILDER
//----------------------------------------------------------
/**
 * bilder einlesen
 * namensteile rausfiltern
 * in gruppen aufteilen(arrays?)
 * die gruppen in die jeweiligen selections appenden
*/

let check = true;
let finishCheck = false;

const exists = (imgArr, img, i) => {
  imgArr.push(img);
  i++;
  check = true;
}

const doesntExist = () => {
  finishCheck = true;
}

// const loadImages = () => {
//   let i = 1;
//   const faceSelectionURL = `../media/img/BananaParts/Face/Banane_${i}b.svg`;
//   let img;
//   let imgArr = [];

//   if(finishCheck) {
//     return;
//   }

//   while (!finishCheck) {
//     check = false;
//     console.log(i);

//     img = new Image();
//     img.onload = exists(imgArr, img, i);
//     img.onerror = doesntExist();
//     img.src = faceSelectionURL;

//     console.log(img);
//   }

// }
// // while(check) {
//   loadImages();
// // }