const $ = data => document.querySelector(data);
const $$ = data => document.querySelectorAll(data);

const btn = $('a');
const triesNeeded = Math.round(Math.random() * 10);
let tries = 0;

btn.addEventListener('mouseenter', () => {
  tries++;

  if (tries == triesNeeded) {
    window.open("../index.html", "_self");
  }

  const width = visualViewport.width;
  const height = visualViewport.height;

  btn.style.left = width - Math.round(Math.random() * (width - btn.offsetWidth) + btn.offsetWidth) + "px";
  btn.style.top = height - Math.round(Math.random() * (height - btn.offsetHeight) + btn.offsetHeight) + "px";
})