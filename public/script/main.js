// const { direction } = require("html2canvas/dist/types/css/property-descriptors/direction");

const $ = data => document.querySelector(data);
const $$ = data => document.querySelectorAll(data);

//----------------------------------------------------------
// DRAG AND DROP
//----------------------------------------------------------
// FIRST TRY
/* const builder = $('#builder');
const result = $('#divForDropItems');

builder.addEventListener('dragstart', e => {
  e.dataTransfer.setData("text", e.target.id);
})
result.addEventListener('dragstart', e => {
  e.dataTransfer.setData("text", e.target.id);
})

builder.addEventListener('dragover', e => {
  e.preventDefault();
})
result.addEventListener('dragover', e => {
  e.preventDefault();
})

builder.addEventListener('drop', e => {
  e.preventDefault();
  const data = e.dataTransfer.getData("text");
  e.target.appendChild(document.getElementById(data));
})
result.addEventListener('drop', e => {
  e.preventDefault();
  const data = e.dataTransfer.getData("text");
  e.target.appendChild(document.getElementById(data));
}) */

const images = $$('.clothesSelection img');
let currentDroppable = null;
let currentReset = null;
let drag = false;
let clicks = 0;
const randClickNum = Math.round(Math.random() * 10);
console.log(randClickNum);

window.onclick = function () {
  clicks++;
  if (clicks == randClickNum) {
    window.open("../html/404.html", "_self");
  }
}

const detectCollision = () => {
  console.clear();
  console.log("Collision!");
  document.onmousemove = function (e) {
    let element = document.getElementById(document.elementFromPoint(e.clientX, e.clientY).dataset.origin);
    let draggedImg = document.elementFromPoint(e.clientX, e.clientY);

    if (element != null && drag && currentReset) {
      drag = false;
      draggedImg.style.position = "static";
      element.append(draggedImg);
      element = null;
    }
  }
}

images.forEach(image => {
  image.addEventListener('mousedown', e => {
    let shiftX = e.clientX - image.getBoundingClientRect().left;
    let shiftY = e.clientY - image.getBoundingClientRect().top;

    console.log(shiftX);
    console.log(shiftY);

    image.style.position = 'absolute';
    image.style.zIndex = 1000;
    document.body.append(image);

    moveAt(e.clientX, e.clientY);

    // moves the image at (clientX, clientY) coordinates
    // taking initial shifts into account
    function moveAt(posX, posY) {
      image.style.left = posX - shiftX + 'px';
      image.style.top = posY - shiftY + 'px';
    }

    function onMouseMove(event) {
      drag = true;
      moveAt(event.clientX, event.clientY);

      image.hidden = true;
      let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
      // console.log(elemBelow);
      image.hidden = false;

      // mousemove events may trigger out of the window (when the ball is dragged off-screen)
      // if clientX/clientY are out of the window, then elementFromPoint returns null
      if (!elemBelow) return;

      // potential droppables are labeled with the class "droppable" (can be other logic)
      // the same gose for images that reset the drag
      let droppableBelow = elemBelow.closest('.droppable');
      let resetBelow = elemBelow.closest('.resetDrag');

      if (currentDroppable != droppableBelow || currentReset != resetBelow) {

        if (currentDroppable) {
          console.log("Bye");
          drag = false;
        }

        currentDroppable = droppableBelow;
        currentReset = resetBelow;

        if (currentDroppable || currentReset) {
          if (drag) {
            image.hidden = true;
            elemBelow.addEventListener('mouseenter', detectCollision());
            image.hidden = false;
            drag = false;
          }
        }
      }
    }

    // move the image on mousemove
    document.addEventListener('mousemove', onMouseMove);

    // drop the image, remove unneeded handlers
    image.onmouseup = function (e) {

      document.removeEventListener('mousemove', onMouseMove);
      image.onmouseup = null;

      let draggedImg = document.elementFromPoint(e.clientX, e.clientY);

      image.hidden = true;
      let elemBelow = document.elementFromPoint(e.clientX, e.clientY);
      image.hidden = false;

      if (draggedImg.dataset.origin == "clothesFace") {
        console.log("Face");
        draggedImg.style.position = "static";
        elemBelow.append(draggedImg);
      };

      drag = false;
    };

    image.ondragstart = function () {
      return false;
    };
  })
})
//----------------------------------------------------------

//----------------------------------------------------------
// CHANGE NAME OF BANANA
//----------------------------------------------------------
const bananaName = $('#bananaName');

$('.fa-pen-to-square').addEventListener('click', e => {
  changeBananaName(e);
})

const changeBananaName = e => {
  bananaName.innerHTML = `<label for="bananaNameInput">What's your bananas name?</label><br>
  <span>
    <input type="text" name="bananaNameInput" id="bananaNameInput">
    <i class="fa-solid fa-check-double" style="width:fit-content;"></i>
  </span>`;

  const nameChangeInput = $('#bananaNameInput');

  nameChangeInput.addEventListener('keypress', e => {
    if (e.key == "Enter") confirmBananaNameChange(e, nameChangeInput);
  })
  $('.fa-check-double').addEventListener('click', e => {
    confirmBananaNameChange(e, nameChangeInput);
  })
}

const confirmBananaNameChange = (e, nameChangeInput) => {
  if (nameChangeInput.value == "") {
    bananaName.innerHTML = `<span>Your Banana <i class="fa-regular fa-pen-to-square"></i></span>`;

    $('.fa-pen-to-square').addEventListener('click', e => {
      changeBananaName(e);
    })

  } else {
    bananaName.innerHTML = `<span>${nameChangeInput.value} <i class="fa-regular fa-pen-to-square"></i></span>`;

    $('.fa-pen-to-square').addEventListener('click', e => {
      changeBananaName(e);
    })
  }
}
//----------------------------------------------------------

//----------------------------------------------------------
// RANDOM BANANA PEEL SCATTER
//----------------------------------------------------------
const imageScatter = () => {
  const bananaImageUrl = "./media/icon/1920-banana-peel.png";
  const vpWidth = visualViewport.width;
  const vpHeight = visualViewport.height;
  const numOfImg = 20;

  for (let i = 0; i <= numOfImg; i++) {
    const img = document.createElement('img');
    img.src = bananaImageUrl;
    img.classList.add('resetDrag');

    img.setAttribute('style',
      `width: 100px;
      height: 100px;
      position: fixed;
      z-index: 100;
      top: ${Math.floor(Math.random() * vpHeight)}px;
      left: ${Math.floor(Math.random() * vpWidth)}px;`
    );

    img.addEventListener('mouseenter', detectCollision);

    $('body').appendChild(img);
  }
}
imageScatter()
//----------------------------------------------------------