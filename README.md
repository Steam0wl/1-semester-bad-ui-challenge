# Bad UI Challenge ![badge -> in progress](https://img.shields.io/badge/status-in_progress-green) ![badge -> version](https://img.shields.io/badge/version-0.1.0-blue)

## Premise
One of the projects we did in our first semester in our apprenticeship, was a "Bad UI Challenge" where we had to research, plan and implement a [website](https://1-semester-bad-ui-challenge-steam0wl-57e7055efbed473e41a9fc9f17.gitlab.io/) that we had to spike with intentionally bad UI.

## Features
- `Custom Drag'n'Drop`  
Users can drag images of faces and clothes on a plain banana with legs. Nowhere else than on the body of the banana, can the item be dropped off.  
![GIF of randomly scattered bananas](media/custom-drag-and-drop.gif){width="50%"}

- `Randomly Placed Banana Peels`  
The UX gets enhanced by randomly scattered banana peels, that will change their positioning on every reload of the page.  
![GIF of randomly scattered bananas](media/random-bananas.gif){width="50%"}

- `Random Reset`  
Dragging an item over one of the banana peels, results in the drag being reset and the item being set back to it's starting location.  
![GIF of randomly scattered bananas](media/drag-reset.gif){width="50%"}

- `404 Redirection`  
Too much clicking will result in the user being redirected to a fake 404 page.  
![GIF of randomly scattered bananas](media/404-redirect.gif){width="50%"}

- `Button Hunt`  
On the 404 page the user has to hunt a rogue button, that redirects the userback to the original page, once the mouse comes in contact with the button about 2-10 times.  
![GIF of randomly scattered bananas](media/button-hunt.gif){width="50%"}

- `Banana theme`  
The whole website is banana themed and colourful.  
<!--![GIF of randomly scattered bananas](media/random-bananas.gif){width="50%"}-->

## Requirements
- A browser that supports HTML5
- JavaScript must be enabled

## Techstack
<table>
  <tr>
    <th><h4>Frontend</h4></th>
    <td>
        <table>
            <tr>
                <th>HTML5</th>
                <th>CSS 3</th>
                <th>JavaScript</th>
            </tr>
            <tr>
                <td>![HTML5 logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg "HTML5 is the latest version of the Hypertext Markup Language used for creating and structuring content on the web, offering enhanced multimedia and interactive capabilities."){:target="_blank" width="70px"}</td>
                <td>![CSS 3 logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg "CSS3 is a stylesheet language used for enhancing the presentation and layout of HTML elements on web pages, providing advanced styling and design options."){:target="_blank" width="70px"}</td>
                <td>![JavaScript logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg "JavaScript is a versatile, high-level programming language used in web development to add interactivity, manipulate web page content, and create dynamic, client-side experiences."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Version Control</h4></th>
    <td>
        <table>
            <tr>
                <th>Git</th>
                <th>GitLab</th>
            </tr>
            <tr>
                <td>![Git logo & description](https://user-content.gitlab-static.net/508f1d7c24450f659a61126ba28ac1082af5cf85/68747470733a2f2f63646e2e6a7364656c6976722e6e65742f67682f64657669636f6e732f64657669636f6e2f69636f6e732f6769742f6769742d6f726967696e616c2e737667 "Git is a distributed version control system used for tracking changes in source code, enabling collaboration among developers and efficient management of software projects."){:target="_blank" width="70px"}</td>
                <td>![GitLab logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original.svg "GitLab is a web-based platform for version control, continuous integration, and collaboration that simplifies the software development lifecycle."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>Documentation & Presentation</h4></th>
    <td>
        <table>
            <tr>
                <th>Markdown</th>
            </tr>
            <tr>
                <td>![Markdown logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/markdown/markdown-original.svg "Markdown is a lightweight markup language that allows you to format plain text documents using simple and easy-to-read syntax."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <th><h4>IDE</h4></th>
    <td>
        <table>
            <tr>
                <th>VS Code</th>
            </tr>
            <tr>
                <td>![VS Code logo & description](https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg "Visual Studio Code is a popular open-source code editor with a range of extensions, designed for efficient coding and customization."){:target="_blank" width="70px"}</td>
            </tr>
        </table>
    </td>
  </tr>
</table>

